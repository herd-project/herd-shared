module gitlab.com/herd-project/herd-shared

go 1.13

require (
	github.com/TV4/logrus-stackdriver-formatter v0.1.0
	github.com/bluele/gcache v0.0.0-20190518031135-bc40bd653833
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/kr/pretty v0.1.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.2.2
)
