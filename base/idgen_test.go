package base_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/herd-project/herd-shared/base"
)

func TestIDGen(t *testing.T) {
	var idGen base.IDGen = base.UUIDIDGen{}

	id1 := idGen.StringID()
	id2 := idGen.StringID()

	assert.NotEmpty(t, id1)
	assert.NotEmpty(t, id2)
	assert.NotEqual(t, id1, id2)
}
