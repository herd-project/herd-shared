package herdlog_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/herd-project/herd-shared/herdlog"
)

func TestLogWithContext(t *testing.T) {
	ctx := context.Background()

	assert.Equal(t, herdlog.L, herdlog.FromContext(ctx))

	newEntry := herdlog.L.WithField("test", "test")
	ctx = herdlog.WithLogger(ctx, newEntry)

	assert.Equal(t, newEntry, herdlog.FromContext(ctx))
}