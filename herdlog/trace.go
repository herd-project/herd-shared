package herdlog

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
)

type Trace struct {
	Log   *logrus.Entry
	name  string
	start time.Time
}

func (t *Trace) Done() {
	elapsed := time.Since(t.start)
	t.Log.WithField("ts", elapsed).Infof("%s: done, time elapsed %v", t.name, elapsed)
}

func NewTrace(ctx context.Context, name string) *Trace {
	l := FromContext(ctx).WithField("trace-name", name)
	return newTraceWithLogEntry(ctx, name, l)
}

func NewTraceWithField(ctx context.Context, name string, key string, value string) *Trace {
	l := FromContext(ctx).WithField("trace-name", name).WithField(key, value)
	return newTraceWithLogEntry(ctx, name, l)
}

func NewTraceWithFields(ctx context.Context, name string, fields logrus.Fields) *Trace {
	l := FromContext(ctx).WithField("trace-name", name).WithFields(fields)
	return newTraceWithLogEntry(ctx, name, l)
}

func newTraceWithLogEntry(ctx context.Context, name string, l *logrus.Entry) *Trace {
	l.Infof("%s: entry", name)

	return &Trace{
		Log:   l,
		name:  name,
		start: time.Now(),
	}
}
